+++
categories = ["tutorial"]
date = "2017-03-08T14:53:17+00:00"
description = "This is my collection of notes as I experience them throughout my career. CAN, is Computing, Algorithms, and Networking"
draft = true
series = ["CAN"]
tags = ["tutorial", "coding", "concepts"]
title = "CAN: Introduction"

+++
Introduction
============

I, Christopher Lee Murray, am a Linux Engineer. My prefered environment has been Fedora 25, which uses the GNOME Desktop Environemnt.
I have decided to compile a collection of my interpretations on Computing. With this being said, I am making it publically available here on my blog, as well as on GitBook.
This compilation, "Computing, Algorithms, and Networking: with examples in Python, Go, JavaScript and Java",  should serve as  a minimal guide to becoming a developer in computing during this era.
My main motivation is to use it as notes, of my experiences and interpretations, which I use as I go through my career.
The structure of these notes will go as follows.

- BASICS
    + variables and arguments
    + arithmetic
    + logics
    + loops and recursion

- INTERMEDIATE
    + Packages
    + Structures vs Objects
    + Polymorphism
    + Time Complexity
    + user interaction design and implementation
        * CLI
        * GUI

- DATA STRUCTURES and ALGORITHMS: common in computing and math

- SYSTEMS PROGRAMMING: Linux

- DATA MANAGEMENT
    + Relational DB
    + Object DB

- STATISTICS

- NETWORKING/ SOCKET PROGRAMMING
    + TCP/IP Protocols Summary
    + Socket Programming
    + Data Streaming
        * text
        * audio
        * video


- WEB PROGRAMMING
    + Architecture
    + Templating
    + Layout
    + Responsive Design
    + Styling

- MACHINE LEARNING

- MULTI-PLATFORM DEVELOPMENT: using WebKit and native libraries.

- SOFTWARE ENGINEERING
    + Management
    + Specifications
    + Design Patterns
    + Modeling
    + Testing
    + Versioning, with git
    + DevOps
