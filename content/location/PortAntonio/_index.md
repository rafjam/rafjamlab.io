---
title: "Port Antonio"
draft: false
code: "portantonio"
type: "location"
map: "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29080.053507246845!2d-76.48548289255467!3d18.174290957349292!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8edb2d32108c50df%3A0x154e5954834c034f!2sRafJam+Bed+and+Breakfast%3A+Port+Antonio!5e0!3m2!1sen!2sjm!4v1497452706486"
map-share: "https://www.google.com/maps/place/RafJam+Bed+and+Breakfast:+Port+Antonio/@18.1703141,-76.4954729,14.2z/data=!4m5!3m4!1s0x8edb2d32108c50df:0x154e5954834c034f!8m2!3d18.1684096!4d-76.4612924"
---

## RafJam: Port Antonio


# Directions



### A Letter from Susan

Dear Valued Guest,


> Welcome to Rafjam’s Bed & Breakfast Port Antonio! Thank you for making Rafjam’s
B&B: Port Antonio your preferred choice in the luscious and unspoiled parish of
Portland. We, the team members of Rafjam’s B&B, assure you that our constant
mission is to provide you with the highest standards of service and comfort
to make your experience truly exceptional. Please, should you require any
additional help or information during your stay, do not hesitate to contact our
General Manager at 1-876-


Best Regards,


Hotel Management



---




# Amenities
> RafJam Port Antonio is one of three (3) Rafjam branches in Jamaica -- namely, Rafjam Irish Town, Rafjam Kingston and Rafjam Port Antonio.
Featuring free WiFi, a Bar, Spa centre, nearby running river and springs, Rafjam Port Antonio offers relaxing & friendly accommodations in Port Antonio. Guests can also enjoy the on-site restaurant.
A flat-screen TV with cable channels, as well as a computer is provided. A balcony or patio is featured in certain rooms.
There is a 24-hour front desk, a shared lounge and gift shop at the property. Breakfast is served between the hours of 8am to 10am.
This bed and breakfast is located near a River where guests can enjoy a relaxing swim or sit at the mini falls while the rushing water beats at your back giving you a soothing massage.


# Facilities of Rafjam Port Antonio
- Sun terrace
- River
- Terrace
- Garden
- Spa and wellness centre (surcharge)
- Pets are allowed. Charges may be applicable.
- Hiking
- Library
- Bicycle rental (surcharge)
- Babysitting/child services (surcharge)
- Laundry (surcharge)
- Fax/photocopying

### General
- Shared lounge/TV area
- Airport shuttle (surcharge)
- Shuttle service (surcharge)
- Designated smoking area
- Gift shop
- Safety deposit box
- Family rooms
- Non-smoking rooms

**Check-in 3pm**

**Check-out 12 noon**

*Cancellation/Prepayment*











# Places to Dine/ Restaurants in Port Antonio

- Dickie’s Best kept Secret - West of Port Antonio, 5 minutes drive from Rafjam Tele: 876-
The Restaurant offers five course meals which must be ordered well in advance. Lunches are also offered, as are Jamaican and European breakfasts.

- Italian Job-29 Harbour Street- 10 minutes drive from Rafjam –tele: 573-8603
A real authentic Italian restaurant and Pizzeria specialist in Pizza, Pasta and Seafood. Open Tuesdays to Saturday from 12 noon to 10pm

- Marribelle’s Restaurant by the Errol Flynn Marina, 10 minutes drive from Rafjam
 Tele: 413-9731 Located by the beautiful harbor of Port Antonio, this restaurant is the ideal place to dine whiles enjoying the view of the Sea.

- Pizza Corner – 2A Harbour Street 10 minutes drive from Rafjam. Tele: 855-7705
This is a rustic Italian Restaurant located in the heart of Port Antonio. Open from 10am to 10 pm. Mondays to Saturdays.













# Places to go in Port Antonio
- Rio Grande Rafting
- Sommer Set Falls
- Shan Shy Beach
- Frenchman’s Cove Beach
- San San Beach
- Blue Lagoon Rafting and Boat rides
- Winifred Beach
- Boston Beach
- Boston Jerk centers
- Reach Falls
- Rio Grande valley tours
- Blue Mountain Bicycle tours
- Scuba diving with Lady Gadiva
- Portland’s Craft market
- The Mussgrave Market



### Entertainment/ Nightlife
- Cristal Night Club
- La Best sports bar
- Vintage Sunday’s at Drapers






# Emergency Contacts
- Port Antonio Police Department – 1876-993-2546
- Port Antonio Fire department – 1876-993-2525
- Port Antonio Hospital – 1876-993-2646

