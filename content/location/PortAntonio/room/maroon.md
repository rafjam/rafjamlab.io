---
location: "portantonio"
title: "Maroon"
code: "maroon"
cost: "USD $50+"
attributes: [
"1 double bed",
"1 bathroom"
]
amenities: [
"wifi",
"tv",
"ac_unit",
]

---

double room ensuite or shared
