---
location: "kingston"
title: "Breadfruit"
code: "breadfruit"
cost: "USD $50+"
attributes: [
"1 double bed",
"1 bathroom"
]
amenities: [
"wifi",
"tv",
"ac_unit",
]

---
