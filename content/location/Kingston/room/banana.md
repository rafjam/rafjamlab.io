---
location: "kingston"
title: "Banana"
code: "banana"
cost: "USD $50+"
attributes: [
"1 double bed",
"1 bathroom"
]
amenities: [
"wifi",
"tv",
"ac_unit",
]

---

has it's own private bathroom, equiped with a hot shower
