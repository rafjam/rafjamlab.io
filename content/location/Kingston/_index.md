---
title: "Kingston"
draft: false
code: "kingston"
type: "location"
map: "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22679.017772327636!2d-76.78996134994786!3d18.005175172803433!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8edb15865afb5cc3%3A0xf9f6b7b55641823!2sRafJam+Bed+and+Breakfast%3A+Kingston!5e0!3m2!1sen!2sjm!4v1497452463378"
map-share: "https://www.google.com/maps/place/RafJam+Bed+and+Breakfast:+Kingston/@18.014338,-76.8009489,14.69z/data=!4m5!3m4!1s0x8edb15865afb5cc3:0xf9f6b7b55641823!8m2!3d18.007343!4d-76.783418"
---

## RafJam: Kingston

Rafjam Kingston is in the heart of New Kingston, 10 minutes or less to all the
shops in new Kingston. 2 minutes walk to Spanish court hotel and 25 mins away
from Norman Manley International Airport . With an efficient team, great
location, cozy rooms and fantastic price, you won't regret staying here!


# Contact Us

1-876-897-4767 or 1-876-426-3667
email: rafjamkingston@gmail.com
