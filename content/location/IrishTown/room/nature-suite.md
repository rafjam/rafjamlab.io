---
location: "irishtown"
title: "Nature Suite"
code: "nature-suite"
cost: "USD $35+"
attributes: [
"1 queen sized bed",
"1 bathroom"
]
amenities: [
"wifi",
"tv",
"ac_unit",
"nature_people",
]

---

The Nature Suite is an ensuite bedroom and its own private bathroom with hot shower.​​​
It also has a small fully equipped kitchenette (fridge, stove, microwave). It sleeps 3 persons and has free wifi access.
