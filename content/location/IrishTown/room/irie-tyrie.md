---
location: "irishtown"
title: "Irie Tyrie"
code: "irie-tyrie"
cost: "USD $150+"
attributes: [
"2 double bed",
"1 bathroom",
"living room",
"small kitchen facilities",
]
amenities: [
"wifi",
"tv",
"nature_people",
]

---

Irie Tyrie- It sleeps up to 4 persons in 2 double beds, 1 bathroom, a balcony and
private garden area. The cost per night, for 2 persons is US$150, inluding breakfast. Each
additional person (2) has an extra charge of US$35 per night.
