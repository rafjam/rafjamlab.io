---
location: "irishtown"
title: "I&I Will"
code: "ini-will"
cost: "USD $200+"
attributes: [
"loft: 2 twin beds",
"bedroom: 2 beds",
"2 bathrooms"
]
amenities: [
"wifi",
"tv",
"nature_people",
]
---

I&I Will- US$200 per night, for 4 persons. It can accommodate 4 additional persons
at the extra cost of US$35 per person per night. It has a loft with 2 twin beds, a bedroom with 2
queen size beds, 2 bathrooms, a balcony, a kitchenette, small living area, private garden and
private pool.
