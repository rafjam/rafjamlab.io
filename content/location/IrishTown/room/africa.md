---
location: "irishtown"
title: "Africa"
code: "africa"
cost: "USD $50+"
attributes: [
"1 double bed",
"1 bathroom",
]
amenities: [
"wifi",
"nature_people",
]
cost-detail: [
"USD $70 (single occupancy)",
"USD $50 (double occupancy)",
]

---

> The Africa Room is a charming dark wood room overlooking the river.
