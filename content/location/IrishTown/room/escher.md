---
location: "irishtown"
title: "Escher"
code: "escher"
cost: "USD $40+"
attributes: [
"1 double bed",
"1 single bed",
"1 bathroom",
]
amenities: [
"wifi",
"nature_people",
]

---

Escher is a large ensuite bedroom opening on a balcony. It has its own bathroom.
It is equipped with fridge and tv and has free wifi access.
It is a bright and relaxing room that can accommodate up to 3 guests.
