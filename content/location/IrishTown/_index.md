---
title: "Irish Town"
draft: false
code: "irishtown"
type: "location"
map: "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d49888.12360570131!2d-76.76774892709237!3d18.034515453981143!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8edb395f27577cf5%3A0xaf1f2d49dba83809!2sRafJam+Bed+and+Breakfast%3A+Irish+Town!5e0!3m2!1sen!2sjm!4v1497451859148"
map-share: "https://www.google.com/maps/place/RafJam+Bed+and+Breakfast:+Irish+Town/@18.0530408,-76.7495336,14.49z/data=!4m5!3m4!1s0x8edb395f27577cf5:0xaf1f2d49dba83809!8m2!3d18.0641483!4d-76.718452"
---

## RafJam: Irish Town

> Lost in the magnificent Blue Mountains, Springdale Cove is a group of houses and apartments. ​
Between two rivers, the houses are in the middle of the nature. Protected from the breeze by the hills and
warmed up by the sun, the cove is a little paradise. Away from the noise and the heat of Kingston, you will
find here quietness and freedom.


# Directions

### From Kingston
#### By Bus/taxi​

> From Papine (north east exit of Kingston), at the end of Old Hope Road.
Go to the Irish Town/Red Light taxi/bus stand located between the Parkview
Pharmacy and the Parkview Supermarket (just after the gas station).
You will need to come out at Red Light (ask the driver). From there it is a
short walk down and up to Rafjam (15 mins). Signs are on the road.
The journey from Papine to Rafjam is a bit less than one hour, including the walk.
The cost per person is J$100 (US$1) but if you have a lot of bags, the driver might
want to ask to pay an additional fee (as it might forbid him to take an additional passenger).
If you would like us to meet you in Red Light, just let us know when you are leaving Papine.

#### By car​​
> From Papine (north east exit of Kingston), at the end of Old Hope Road.
After passing the petrol station, take the first left turn onto Gordon Town road,
and then the fourth left turn onto Irish town road (first on the left, going up,
after you pass the bridge). You will see, in addition to ours, signs for Strawberry
Hill or Craighton estate, follow these signs all the way to Redlight square (5 minutes after Irish Town).
Entering Red Light, on your left, you will see a large Rafjam Bed and Breakfast sign.
Turn right on Hopewell road, the dirt road going down, and follow the road straight
until you get to us, at the very end of it.
Count on about 40 mins drive for the journey.
If you would like us to meet you in Red Light, just let us know when you are
leaving Papine.


### From the North Coast by the mountain road (through Buff Bay)

> _Note: the road has very little traffic and offers beautiful sceneries. But the
conditions of the road are quite bad in some places and a 4X4 is recommended
(but not mandatory). If you are not totally comfortable with the Jamaican roads
we advise you to drive through Annotto Bay - Castleton - Stony Hill -
Kingston or through St Thomas on the East._

#### In Buff Bay, on the main square, take the road opposite the Texaco gas station, direction Charles Town.

> Stay on that road for the most part of the trip. You will follow the river on
your right then cross it and continue the road with it on your left. Once on that
side, you will start the real climb of the mountains. You will pass through some little picturesque communities.
Near the summit, you will see the road closed, following a landslide during a
2004 hurricane. There is a detour road on your right, climbing steep through a coffee farm.
On the other side, you will reach the little community of Section where the
road splits in 2. To the left leads you deeper in the mountains. Take the right
one. After about 15-20 mins you will see on your right the entrance of the beautiful
Hollywell Park. From there, you can have, if the weather is clear, one of the most beautiful vista over Kingston.
Continue before passing through the Newcastle Army training Center.
Another 15-20 mins drive will lead you to Red Light. At the other end of it, take
the dirt road going down on your left and follow the signs to Rafjam.
Count on about 2 hours drive for the journey.
If you would like us to meet you in Red Light, just let us know when you are passing Newcastle.
